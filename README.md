#First Commit

To run the project, npm install

Basics components - Done
Basic Style - Done
Fetch api data - In progress
Basic Routing - In progress
Mobile/tables resposiviness - In progress

New components

-Product
-Produc_Page
-Footer
-Vitrine

#Product

This component will be using routes and data from the api to render dynamically. Its hardcoded for now, for better comprehension.
From this components you will be able to consult the availbiality of the product or buy. (go to Product_Page)

#Product_Page

This component is rendered from the props of the Product component, and will be the last step before the user buy the product.

#Footer

This component will prompt the user to type and send their e-mail / name to aquire advantages and also sing up for the newsletter

#Vitrine

This component is rendered with the dynamically rendered Products components, have only one hard coded Product for better comprehension.

_______________________________________________________________________________________________________________________________________

#Second Commit

React-toastify - Added this lib for easy and quick validation for the newsletter, its a well known and broadly accepted lib.

Basics components - Done
Style - Done
Fetch api data - In progress (since i dont have permission to acess the endpoint, i used the catalogo.json to emulate the api data fetch)
Routing - Done (Because of the acess issue i left all api routes and code, commented)
Mobile/tables resposiviness - Done

Removed:

Product_Page - Since you'll be redirected to the sellers page in either cases of availbility or not.

Added:
-Newsletter: You can sign in for newsletter, with e-mail and name validation. The validation is coded for native browser and using toastify also.
-Buy/Consult buttons : Show on hover effect on web, always show on mobile. The buttons redirect to the seller page either to the add to cart page 
in cases of "Buy" or to the product page in cases of "Consult"

__________________________________________________________________________________________________________________________________________


